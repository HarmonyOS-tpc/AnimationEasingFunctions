/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package daimajia.com.easing;

import daimajia.com.easing.ResourceTable;
import com.daimajia.easing.BaseEasingMethod;
import com.daimajia.easing.Glider;
import com.daimajia.easing.Skill;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

/**
 *  主页面
 */
public class MainAbility extends Ability {
    private ListContainer mEasingList;
    private EasingAdapter mAdapter;
    private Component mTarget;
    private long mDuration = 2000;

    private DrawView mHistory;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_my);
        mEasingList = (ListContainer) findComponentById(ResourceTable.Id_easing_list);
        mAdapter = new EasingAdapter(this);
        mEasingList.setItemProvider(mAdapter);
        mTarget = findComponentById(ResourceTable.Id_target);
        mHistory = (DrawView) findComponentById(ResourceTable.Id_history);
        mEasingList.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long var4) {
                mHistory.clear();
                Skill skill = (Skill) component.getTag();
                // 动画集合
                AnimatorGroup set = new AnimatorGroup();
                mTarget.setTranslationX(0);
                mTarget.setTranslationY(0);
                // 数值动画
                AnimatorValue animator = new AnimatorValue();
                animator.setDuration(mDuration);
                BaseEasingMethod easing = skill.getMethod(mDuration);
                // 画最开始的点
                mHistory.drawPoint(0, mDuration, 0 - vpToPixels(getContext(), 60));
                set.runSerially(
                        Glider.glide(skill, mDuration, animator,new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float value) {

                        float start = 0;
                        float end = -vpToPixels(getContext(), 157) ;
                        float result = easing.evaluate(value, start, end);
                        mTarget.setTranslationY(result);
                        mHistory.drawPoint(mDuration * value, mDuration, result - vpToPixels(getContext(), 60));
                    }
                }));
                set.setDuration(mDuration);
                set.start();
            }
        });

    }

    /**
     * vp转px
     *
     * @param context   上下文
     * @param vpValue vp数值
     * @return px
     */
    public static float vpToPixels(Context context, float vpValue) {
        return  AttrHelper.fp2px(vpValue,context);
    }
}
